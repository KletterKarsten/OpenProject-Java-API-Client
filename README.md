Syntax Validation result of Api-Description: ![validation badge](http://online.swagger.io/validator?url=https://gitlab.com/kaneo-gmbh/OpenProject-Java-API-Client/raw/master/openProject_swagger.yaml)

# Update API-Client

1. update API Description in [openProject_swagger.yaml](https://gitlab.com/kaneo-gmbh/OpenProject-Java-API-Client/blob/master/openProject_swagger.yaml)
2. validate API Description syntax
```
wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
java -jar swagger-codegen-cli.jar validate -i openProject_swagger.yaml
```
3. increase artifactVersion in [swagger-codegen-config.json](https://gitlab.com/kaneo-gmbh/OpenProject-Java-API-Client/blob/master/swagger-codegen-config.json)
4. generate java client

``` sh
cd OpenProject-Java-API-Client
# clean up
rm generate-client -rf
wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
java -jar swagger-codegen-cli.jar generate -l java -c swagger-codegen-config.json -i openProject_swagger.yaml  -s -o generated-client/
patch generated-client/build.gradle build.gradle.patch
```

# Build jar

``` sh
cd generated-client
chmod +x gradlew 
gradlew assemble
```
jars are placed to ./generated-client/build/libs/

# Use OpenProject-API-client

 - download [latest artifacts](https://gitlab.com/kaneo-gmbh/OpenProject-Java-API-Client/-/jobs/artifacts/master/download?job=build) or build yourself
 - copy jars to local libs folder
 - add these dependencys

``` gradle
    compile 'org.openproject:openproject-api-client:0.2.0'// placed in local libs folder
    compile 'org.openproject:openproject-api-client:0.2.0:sources'// placed in local libs folder
    compile 'com.squareup.okhttp:okhttp:2.7.5'
    compile 'com.squareup.okhttp:logging-interceptor:2.7.5'
    compile 'com.google.code.gson:gson:2.8.1'//
    compile 'io.gsonfire:gson-fire:1.8.0'
```

 - configure Api Access
``` java
public class OpenProjectConnection {

	private static final String SERVER_BASE_PATH = "http://example.org";
	private static final String APIKEY = System.getProperty("apiKey");
	private static final String USERNAME = "apikey";

	public static void configureApiAccess() {
		ApiClient apiClient = new ApiClient();
		apiClient.setBasePath(SERVER_BASE_PATH);
		apiClient.setDebugging(true);
		if (APIKEY == null || APIKEY.isEmpty()) {
			throw new IllegalArgumentException("API Key missing - pass it as parameter on start [ -DapiKey=foobar ]");
		}
		apiClient.setUsername(USERNAME);
		apiClient.setPassword(APIKEY);

		Configuration.setDefaultApiClient(apiClient);
	}
	
	
	public static void main(String[] args) {
		configureApiAccess();

		UsersApi usersApi = new UsersApi();
		User me;
		try {
			me = usersApi.apiV3UsersIdGet("me");
			System.out.println(me.getLogin());
		} catch (ApiException e) {
			System.err.println("Connection Error!");
			e.printStackTrace();
		}
	}
}
```