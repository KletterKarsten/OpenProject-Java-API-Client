
# WPType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**color** | **String** |  |  [optional]
**position** | **Integer** |  |  [optional]
**isDefault** | **Boolean** |  |  [optional]
**isMilestone** | **Boolean** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**links** | [**WPTypeLinks**](WPTypeLinks.md) |  |  [optional]



