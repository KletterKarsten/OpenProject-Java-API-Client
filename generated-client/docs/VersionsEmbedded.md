
# VersionsEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elements** | [**List&lt;Version&gt;**](Version.md) |  |  [optional]



