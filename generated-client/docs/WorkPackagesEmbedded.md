
# WorkPackagesEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elements** | [**List&lt;WorkPackage&gt;**](WorkPackage.md) |  |  [optional]



