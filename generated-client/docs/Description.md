
# Description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | [**FormatEnum**](#FormatEnum) |  |  [optional]
**raw** | **String** |  |  [optional]
**html** | **String** |  |  [optional]


<a name="FormatEnum"></a>
## Enum: FormatEnum
Name | Value
---- | -----
TEXTILE | &quot;textile&quot;



