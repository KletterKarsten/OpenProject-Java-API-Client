
# TimeEntries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**TimeEntriesEmbedded**](TimeEntriesEmbedded.md) |  |  [optional]
**links** | [**TimeEntriesLinks**](TimeEntriesLinks.md) |  |  [optional]



