
# WorkPackagePatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lockVersion** | **Integer** |  |  [optional]
**percentageDone** | **Integer** |  |  [optional]
**subject** | **String** |  |  [optional]
**description** | [**Description**](Description.md) |  |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dueDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**estimatedTime** | **String** |  |  [optional]
**links** | [**WorkPackageLinks**](WorkPackageLinks.md) |  |  [optional]



