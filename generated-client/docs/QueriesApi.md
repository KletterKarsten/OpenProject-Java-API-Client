# QueriesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ProjectsIdQueriesDefaultGet**](QueriesApi.md#apiV3ProjectsIdQueriesDefaultGet) | **GET** /api/v3/projects/{id}/queries/default | View default query for project
[**apiV3ProjectsIdQueriesSchemaGet**](QueriesApi.md#apiV3ProjectsIdQueriesSchemaGet) | **GET** /api/v3/projects/{id}/queries/schema | View schema for project queries
[**apiV3QueriesAvailableProjectsGet**](QueriesApi.md#apiV3QueriesAvailableProjectsGet) | **GET** /api/v3/queries/available_projects | Available projects
[**apiV3QueriesDefaultGet**](QueriesApi.md#apiV3QueriesDefaultGet) | **GET** /api/v3/queries/default | View default query
[**apiV3QueriesFormPost**](QueriesApi.md#apiV3QueriesFormPost) | **POST** /api/v3/queries/form | Query Create Form
[**apiV3QueriesGet**](QueriesApi.md#apiV3QueriesGet) | **GET** /api/v3/queries | List queries
[**apiV3QueriesIdDelete**](QueriesApi.md#apiV3QueriesIdDelete) | **DELETE** /api/v3/queries/{id} | Delete query
[**apiV3QueriesIdGet**](QueriesApi.md#apiV3QueriesIdGet) | **GET** /api/v3/queries/{id} | View query
[**apiV3QueriesIdPatch**](QueriesApi.md#apiV3QueriesIdPatch) | **PATCH** /api/v3/queries/{id} | Edit Query
[**apiV3QueriesIdStarPatch**](QueriesApi.md#apiV3QueriesIdStarPatch) | **PATCH** /api/v3/queries/{id}/star | Star query
[**apiV3QueriesIdUnstarPatch**](QueriesApi.md#apiV3QueriesIdUnstarPatch) | **PATCH** /api/v3/queries/{id}/unstar | Unstar query
[**apiV3QueriesPost**](QueriesApi.md#apiV3QueriesPost) | **POST** /api/v3/queries | Create query
[**apiV3QueriesSchemaGet**](QueriesApi.md#apiV3QueriesSchemaGet) | **GET** /api/v3/queries/schema | View schema for global queries


<a name="apiV3ProjectsIdQueriesDefaultGet"></a>
# **apiV3ProjectsIdQueriesDefaultGet**
> apiV3ProjectsIdQueriesDefaultGet(id, filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, showHierarchies)

View default query for project

Same as [viewing an existing, persisted Query](#queries-query-get) in its response, this resource returns an unpersisted query and by that allows to get the default query configuration. The client may also provide additional parameters which will modify the default query. The query will already be scoped for the project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Id of the project the default query is requested for
String filters = "filters_example"; // String | JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query's persisted filters. All filters also accepted by the work packages endpoint are accepted.
Integer offset = 1; // Integer | Page number inside the queries' result collection of work packages.
Integer pageSize = 56; // Integer | Number of elements to display per page for the queries' result collection of work packages.
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. The sort criteria is applied to the querie's result collection of work packages overriding the query's persisted sort criteria.
String groupBy = "groupBy_example"; // String | The column to group by. The grouping criteria is applied to the to the querie's result collection of work packages overriding the query's persisted group criteria.
Boolean showSums = false; // Boolean | Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie's result collection of work packages overriding the query's persisted sums property.
Boolean timelineVisible = false; // Boolean | Indicates whether the timeline should be shown.
Boolean showHierarchies = true; // Boolean | Indicates whether the hierarchy mode should be enabled.
try {
    apiInstance.apiV3ProjectsIdQueriesDefaultGet(id, filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, showHierarchies);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3ProjectsIdQueriesDefaultGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Id of the project the default query is requested for |
 **filters** | **String**| JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query&#39;s persisted filters. All filters also accepted by the work packages endpoint are accepted. | [optional]
 **offset** | **Integer**| Page number inside the queries&#39; result collection of work packages. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page for the queries&#39; result collection of work packages. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. The sort criteria is applied to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sort criteria. | [optional]
 **groupBy** | **String**| The column to group by. The grouping criteria is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted group criteria. | [optional]
 **showSums** | **Boolean**| Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sums property. | [optional] [default to false]
 **timelineVisible** | **Boolean**| Indicates whether the timeline should be shown. | [optional] [default to false]
 **showHierarchies** | **Boolean**| Indicates whether the hierarchy mode should be enabled. | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsIdQueriesSchemaGet"></a>
# **apiV3ProjectsIdQueriesSchemaGet**
> apiV3ProjectsIdQueriesSchemaGet(id)

View schema for project queries

Retrieve the schema for project queries.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Long id = 789L; // Long | ID of project to return
try {
    apiInstance.apiV3ProjectsIdQueriesSchemaGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3ProjectsIdQueriesSchemaGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| ID of project to return |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesAvailableProjectsGet"></a>
# **apiV3QueriesAvailableProjectsGet**
> apiV3QueriesAvailableProjectsGet()

Available projects

Gets a list of projects that are available as projects a query can be assigned to.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
try {
    apiInstance.apiV3QueriesAvailableProjectsGet();
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesAvailableProjectsGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesDefaultGet"></a>
# **apiV3QueriesDefaultGet**
> apiV3QueriesDefaultGet(filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, timelineZoomLevel, showHierarchies)

View default query

Same as [viewing an existing, persisted Query](#queries-query-get) in its response, this resource returns an unpersisted query and by that allows to get the default query configuration. The client may also provide additional parameters which will modify the default query.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
String filters = "filters_example"; // String | JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query's persisted filters. All filters also accepted by the work packages endpoint are accepted.
Integer offset = 1; // Integer | Page number inside the queries' result collection of work packages.
Integer pageSize = 56; // Integer | Number of elements to display per page for the queries' result collection of work packages.
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. The sort criteria is applied to the querie's result collection of work packages overriding the query's persisted sort criteria.
String groupBy = "groupBy_example"; // String | The column to group by. The grouping criteria is applied to the to the querie's result collection of work packages overriding the query's persisted group criteria.
Boolean showSums = false; // Boolean | Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie's result collection of work packages overriding the query's persisted sums property.
Boolean timelineVisible = false; // Boolean | Indicates whether the timeline should be shown.
String timelineZoomLevel = "days"; // String | Indicates in what zoom level the timeline should be shown. Valid values are  `days`, `weeks`, `months`, `quarters`, and `years`.
Boolean showHierarchies = true; // Boolean | Indicates whether the hierarchy mode should be enabled.
try {
    apiInstance.apiV3QueriesDefaultGet(filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, timelineZoomLevel, showHierarchies);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesDefaultGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | **String**| JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query&#39;s persisted filters. All filters also accepted by the work packages endpoint are accepted. | [optional]
 **offset** | **Integer**| Page number inside the queries&#39; result collection of work packages. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page for the queries&#39; result collection of work packages. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. The sort criteria is applied to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sort criteria. | [optional]
 **groupBy** | **String**| The column to group by. The grouping criteria is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted group criteria. | [optional]
 **showSums** | **Boolean**| Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sums property. | [optional] [default to false]
 **timelineVisible** | **Boolean**| Indicates whether the timeline should be shown. | [optional] [default to false]
 **timelineZoomLevel** | **String**| Indicates in what zoom level the timeline should be shown. Valid values are  &#x60;days&#x60;, &#x60;weeks&#x60;, &#x60;months&#x60;, &#x60;quarters&#x60;, and &#x60;years&#x60;. | [optional] [default to days]
 **showHierarchies** | **Boolean**| Indicates whether the hierarchy mode should be enabled. | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesFormPost"></a>
# **apiV3QueriesFormPost**
> apiV3QueriesFormPost()

Query Create Form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
try {
    apiInstance.apiV3QueriesFormPost();
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesFormPost");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesGet"></a>
# **apiV3QueriesGet**
> apiV3QueriesGet(filters)

List queries

Returns a collection of queries. The collection can be filtered via query parameters similar to how work packages are filtered. Please note however, that the filters are applied to the queries and not to the work packages the queries in turn might return.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + project: filters queries by the project they are assigned to. If the project filter is passed with the `!*` (not any) operator, global queries are returned.
try {
    apiInstance.apiV3QueriesGet(filters);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + project: filters queries by the project they are assigned to. If the project filter is passed with the &#x60;!*&#x60; (not any) operator, global queries are returned. | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesIdDelete"></a>
# **apiV3QueriesIdDelete**
> apiV3QueriesIdDelete(id)

Delete query

Delete the query identified by the id parameter

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Query id
try {
    apiInstance.apiV3QueriesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Query id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesIdGet"></a>
# **apiV3QueriesIdGet**
> apiV3QueriesIdGet(id, filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, timelineLabels, showHierarchies)

View query

Retreive an individual query as identified by the id parameter. Then end point accepts a number of parameters that can be used to override the resources&#39; persisted parameters.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Query id
String filters = "filters_example"; // String | JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query's persisted filters. All filters also accepted by the work packages endpoint are accepted.
Integer offset = 1; // Integer | Page number inside the queries' result collection of work packages.
Integer pageSize = 56; // Integer | Number of elements to display per page for the queries' result collection of work packages.
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. The sort criteria is applied to the querie's result collection of work packages overriding the query's persisted sort criteria.
String groupBy = "groupBy_example"; // String | The column to group by. The grouping criteria is applied to the to the querie's result collection of work packages overriding the query's persisted group criteria.
Boolean showSums = false; // Boolean | Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie's result collection of work packages overriding the query's persisted sums property.
Boolean timelineVisible = false; // Boolean | Indicates whether the timeline should be shown.
String timelineLabels = "{}"; // String | Overridden labels in the timeline view
Boolean showHierarchies = true; // Boolean | Indicates whether the hierarchy mode should be enabled.
try {
    apiInstance.apiV3QueriesIdGet(id, filters, offset, pageSize, sortBy, groupBy, showSums, timelineVisible, timelineLabels, showHierarchies);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Query id |
 **filters** | **String**| JSON specifying filter conditions. The filters provided as parameters are not applied to the query but are instead used to override the query&#39;s persisted filters. All filters also accepted by the work packages endpoint are accepted. | [optional]
 **offset** | **Integer**| Page number inside the queries&#39; result collection of work packages. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page for the queries&#39; result collection of work packages. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. The sort criteria is applied to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sort criteria. | [optional]
 **groupBy** | **String**| The column to group by. The grouping criteria is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted group criteria. | [optional]
 **showSums** | **Boolean**| Indicates whether properties should be summed up if they support it. The showSums parameter is applied to the to the querie&#39;s result collection of work packages overriding the query&#39;s persisted sums property. | [optional] [default to false]
 **timelineVisible** | **Boolean**| Indicates whether the timeline should be shown. | [optional] [default to false]
 **timelineLabels** | **String**| Overridden labels in the timeline view | [optional] [default to {}]
 **showHierarchies** | **Boolean**| Indicates whether the hierarchy mode should be enabled. | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesIdPatch"></a>
# **apiV3QueriesIdPatch**
> apiV3QueriesIdPatch(id, body)

Edit Query

When calling this endpoint the client provides a single object, containing the properties and links that it wants to change, in the body. Note that it is only allowed to provide properties or links supporting the **write** operation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Query id
Body3 body = new Body3(); // Body3 | 
try {
    apiInstance.apiV3QueriesIdPatch(id, body);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesIdPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Query id |
 **body** | [**Body3**](Body3.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesIdStarPatch"></a>
# **apiV3QueriesIdStarPatch**
> apiV3QueriesIdStarPatch(id)

Star query



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Query id
try {
    apiInstance.apiV3QueriesIdStarPatch(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesIdStarPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Query id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesIdUnstarPatch"></a>
# **apiV3QueriesIdUnstarPatch**
> apiV3QueriesIdUnstarPatch(id)

Unstar query



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
Integer id = 56; // Integer | Query id
try {
    apiInstance.apiV3QueriesIdUnstarPatch(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesIdUnstarPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Query id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesPost"></a>
# **apiV3QueriesPost**
> apiV3QueriesPost()

Create query

When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body. The required fields of a Query can be found in its schema, which is embedded in the respective form. Note that it is only allowed to provide properties or links supporting the write operation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
try {
    apiInstance.apiV3QueriesPost();
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesPost");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3QueriesSchemaGet"></a>
# **apiV3QueriesSchemaGet**
> apiV3QueriesSchemaGet()

View schema for global queries

Retrieve the schema for global queries, those, that are not assigned to a project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueriesApi apiInstance = new QueriesApi();
try {
    apiInstance.apiV3QueriesSchemaGet();
} catch (ApiException e) {
    System.err.println("Exception when calling QueriesApi#apiV3QueriesSchemaGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

